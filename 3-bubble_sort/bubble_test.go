package bubble

import (
	"reflect"
	"testing"
)

func Test_swap(t *testing.T) {
	type args struct {
		data []int
		i    int
		j    int
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			swap(tt.args.data, tt.args.i, tt.args.j)
		})
	}
}

func Test_bubbleSort(t *testing.T) {
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "example1:",
			args: args{
				[]int{1, 2, 3, 4, 5, 7, 6},
			},
			want: []int{1, 2, 3, 4, 5, 6, 7},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bubbleSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("bubbleSort() = %v, want %v", got, tt.want)
			}
		})
	}
}
