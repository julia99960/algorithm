package bubble

func swap(data []int, i int, j int) {
	tmp := data[i]
	data[i] = data[j]
	data[j] = tmp
}

func bubbleSort(data []int) []int {
	flag := true
	for i := 0; i < len(data)-1 && flag; i++ {
		flag = false
		for j := 0; j < len(data)-i-1; j++ {
			if data[j+1] < data[j] {
				swap(data, j+1, j)
				flag = true
			}
		}
	}
	return data
}
