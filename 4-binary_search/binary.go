package binary

import (
	"math"
)

func find(k int, list []int) int {
	left, right, mid := 1, len(list), 0
	for {
		mid = int(math.Floor(float64((left + right) / 2)))
		if list[mid] > k {
			right = mid - 1
		} else if list[mid] < k {
			left = mid + 1
		} else {
			break
		}
		if left > right {
			mid = -1
			break
		}
	}
	return mid
}
