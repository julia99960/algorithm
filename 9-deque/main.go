package main

import (
	"errors"
	"fmt"
)

const maxDequeSize = 5

// Deque 雙向佇列
type Deque struct {
	createQ []int
}

// IsFull 判斷佇列已滿
func (dq *Deque) IsFull() bool {
	return len(dq.createQ) == maxDequeSize
}

// Push 加入元素
func (dq *Deque) Push(item int, pos string) (err error) {
	if dq.IsFull() {
		return errors.New("Queue is full")
	}

	if pos == "rear" {
		dq.createQ = append(dq.createQ, item)
	} else {
		dq.createQ = append([]int{item}, dq.createQ...)
	}
	return
}

// IsEmpty 判斷陣列是否為空
func (dq *Deque) IsEmpty() bool {
	return len(dq.createQ) == 0
}

// Pop 取出
func (dq *Deque) Pop(pos string) (err error) {
	if dq.IsEmpty() {
		return errors.New("Queue is empty")
	}

	if pos == "rear" {
		dq.createQ = dq.createQ[:len(dq.createQ)-1]
	} else {
		dq.createQ = dq.createQ[1:]
	}
	return
}

func main() {
	q := &Deque{
		createQ: []int{},
	}

	q.Push(1, "rear")
	fmt.Println(q.createQ)

	q.Push(2, "rear")
	fmt.Println(q.createQ)

	q.Pop("front")
	fmt.Println(q.createQ)

	q.Push(0, "front")
	fmt.Println(q.createQ)
}
