package selection

func selectionSort(a []int) []int {
	b := make([]int, len(a))
	for i := 0; i < len(a); i++ {
		b[i] = a[i]
		for j := i + 1; j < len(a); j++ {
			av := a[j]
			bv := b[i]
			if b[i] >= a[j] {
				b[i] = av
				a[j] = bv
			}
		}
	}
	return b
}
