package main

import (
	"errors"
	"fmt"
	"os"
)

const maxStackSize = 5

// Stack 設定堆疊框格式為slice
type Stack struct {
	S []int
}

func isFull(s []int) bool {
	return len(s) == maxStackSize
}

func isEmpty(s []int) bool {
	return len(s) == 0
}

func (s *Stack) push(item int) (pos int, err error) {
	if isFull(s.S) {
		return 0, errors.New("Stack is Full")
	}
	s.S = append(s.S, item)
	return len(s.S) - 1, nil
}

func (s *Stack) pop() (item int, err error) {
	if isEmpty(s.S) {
		return 0, errors.New("Stack is empty")
	}
	front := s.S[len(s.S)-1]
	s.S = s.S[:len(s.S)-1]
	return front, nil
}

func main() {
	s := &Stack{
		S: []int{},
	}

	var command string
	var item int
	for {
		fmt.Println()
		fmt.Println("-----------------------------")
		fmt.Println("1.新增item到堆疊最上方請輸入push")
		fmt.Println("2.獲取堆疊最後加入的item請輸入pop")
		fmt.Println("3.輸入b退出")
		fmt.Println("-----------------------------")
		fmt.Scanln(&command)

		switch command {
		case "push":
			fmt.Println("輸入你要放入堆疊最上面的數字:")
			fmt.Scanln(&item)
			pos, err := s.push(item)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Printf("將 %v 放入堆疊位置 %v 成功!\n", item, pos)
				fmt.Printf("當前堆疊框：%v", s.S)
			}
		case "pop":
			item, err := s.pop()
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Printf("\n將 %v 取出從堆疊位置成功!\n", item)
				fmt.Printf("當前堆疊框：%v\n", s.S)
			}
		case "b":
			os.Exit(0)
		}
	}
}
