package main

import (
	"fmt"
	"math/rand"
	"time"
)

// Matrix2d 二維矩陣
type Matrix2d struct {
	row   int
	col   int
	max2d [][]int
}

// RandomTo01 隨機回傳0或1
func RandomTo01() int {
	rand.Seed(int64(time.Now().UnixNano()))
	return rand.Int() % 2
}

// Create2dMatrix 建立二維矩陣
func (matrix *Matrix2d) Create2dMatrix() {
	m2d := make([][]int, matrix.row)
	for i := range m2d {
		m1d := make([]int, matrix.col)
		for j := range m1d {
			m1d[j] = RandomTo01()
		}
		m2d[i] = m1d
	}
	matrix.max2d = m2d
}

// Show2dMatrix 印出二維矩陣
func Show2dMatrix(m2d [][]int) {
	for i := range m2d {
		for j := range m2d[i] {
			fmt.Printf("%v ", m2d[i][j])
		}
		fmt.Println()
	}
}

func main() {
	m := &Matrix2d{
		row:   4,
		col:   5,
		max2d: [][]int{},
	}
	m.Create2dMatrix()
	Show2dMatrix(m.max2d)
}
