package main

import "fmt"

// 當前位置
type point struct {
	x int
	y int
}

// 定義可以移動方位的變量
var move = []point{
	{-1, 0},  // N
	{-1, 1},  // NE
	{0, 1},   // E
	{1, 1},   // SE
	{1, 0},   // S
	{1, -1},  // WS
	{0, -1},  // W
	{-1, -1}, // WN
}

// 迷宮地圖
var maze = [][]int{
	{0, 1, 1, 1, 1, 1, 1, 1},
	{1, 0, 1, 1, 0, 0, 0, 1},
	{1, 1, 0, 1, 1, 1, 1, 1},
	{1, 1, 0, 1, 0, 1, 1, 1},
	{1, 1, 1, 0, 0, 0, 0, 1},
	{1, 1, 0, 1, 1, 1, 0, 1},
	{1, 1, 1, 1, 1, 1, 1, 0},
}

// 入口
var start = point{0, 0}

// 出口
var end = point{len(maze) - 1, len(maze[0]) - 1}

// 前進下一個點
func (p point) add(d point) point {
	return point{
		p.x + d.x,
		p.y + d.y,
	}
}

func (p point) at(grid [][]int) (int, bool) {
	// 上下越界
	if p.x < 0 || p.x >= len(grid) {
		return 0, false
	}
	// 左右越界
	if p.y < 0 || p.y >= len(grid[p.x]) {
		return 0, false
	}
	return grid[p.x][p.y], true
}

func show(s [][]int) {
	for _, row := range s {
		for _, val := range row {
			fmt.Printf("%3d", val)
		}
		fmt.Println()
	}
}

// 開始走迷宮
func walk(maze [][]int, start point, end point) [][]int {
	// 創造一個跟迷宮一樣大小的地圖
	var mark = make([][]int, len(maze))
	for i := range mark {
		mark[i] = make([]int, len(maze[0]))
	}

	// 定義一個空的堆疊
	Q := []point{start}

	// 列隊為空,走迷宮結束
	for len(Q) > 0 {
		cur := Q[0] // 要探索的點
		Q = Q[1:]   // 出隊

		// 已到終點
		if cur == end {
			break
		}

		// 遍歷八種方位的走法
		for _, d := range move {

			next := cur.add(d)

			val, ok := next.at(maze)
			if val == 1 || !ok { // 撞牆了
				continue
			}

			val, ok = next.at(mark)
			if val != 0 || !ok { // 已經寫進去就不寫了
				continue
			}

			// 不能走回起点
			if next == start {
				continue
			}

			curMark, _ := cur.at(mark)

			mark[next.x][next.y] = curMark + 1
			Q = append(Q, next)
		}
	}
	return mark
}

func main() {
	s := walk(maze, start, end)
	show(s)
}
