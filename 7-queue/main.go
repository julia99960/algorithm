package main

import "fmt"

// MaxQueueSize 定義最大佇列尺寸
var MaxQueueSize = 3

// Queue 佇列結構
type Queue struct {
	Q []int
}

// IsFullQ 佇列中的元素個數 == MaxQueueSize
func (q *Queue) IsFullQ() bool {
	return len(q.Q) == MaxQueueSize
}

// AddQ 插入一個項目到 queue 中並回傳 queue
func (q *Queue) AddQ(item int) []int {
	if q.IsFullQ() {
		return q.Q
	}
	// 新增item至queue
	q.Q = append(q.Q, item)
	return q.Q
}

// IsEmpty queue元素的數量 = 0
func IsEmpty(q []int) bool {
	return len(q) == 0
}

// DeleteQ 移除並回傳queue前端的項目
func (q *Queue) DeleteQ() int {
	if IsEmpty(q.Q) {
		return 0
	}
	// 移除queue最前端的項目
	slice := q.Q
	q.Q = slice[1:]
	// 當移除到queue最後一個項目時
	if len(q.Q) == 0 {
		return 0
	}
	return q.Q[0]
}

func main() {
	q := &Queue{
		Q: []int{1, 3},
	}
	q.AddQ(5)
	fmt.Println(q.Q)

	q.AddQ(2)
	fmt.Println(q.Q)

	q.AddQ(9)
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)

	q.DeleteQ()
	fmt.Println(q.Q)
}
