package posfix

import "testing"

func Test_eval(t *testing.T) {
	type args struct {
		expr string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "example1",
			args: args{
				"1 2+7*",
			},
			want: 21,
		},
		{
			name: "example2",
			args: args{
				"2 3 4*+",
			},
			want: 14,
		},
		{
			name: "example3",
			args: args{
				"6 2/3-4 2*+",
			},
			want: 8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := eval(tt.args.expr); got != tt.want {
				t.Errorf("eval() = %v, want %v", got, tt.want)
			}
		})
	}
}
