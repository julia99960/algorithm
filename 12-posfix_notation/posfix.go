package posfix

// 輸入後置運算式,計算出答案
func eval(expr string) int {
	stack := []rune{}
	for _, v := range expr {
		if v == ' ' {
			continue
		}
		if v != '+' && v != '-' && v != '*' && v != '/' && v != '%' {
			stack = append(stack, v-'0')
		} else {
			op2 := stack[len(stack)-1]
			op1 := stack[len(stack)-2]
			switch v {
			case '+':
				stack = stack[:len(stack)-2]
				stack = append(stack, op1+op2)
				break
			case '-':
				stack = stack[:len(stack)-2]
				stack = append(stack, op1-op2)
				break
			case '*':
				stack = stack[:len(stack)-2]
				stack = append(stack, op1*op2)
				break
			case '/':
				stack = stack[:len(stack)-2]
				stack = append(stack, op1/op2)
				break
			case '%':
				stack = stack[:len(stack)-2]
				stack = append(stack, op1%op2)
				break
			}
		}
	}
	return int(stack[0])
}
