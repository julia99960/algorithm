package fibonacci

// 費氏數列
func fibonacci(item int) int {
	if item == 0 || item == 1 || item == 2 {
		return item
	}
	return fibonacci(item-1) + fibonacci(item-2)
}
