package insertion

func insertionSort(data []int) []int {
	for i := 1; i < len(data); i++ {
		for j := i; j > 0; j-- {
			if data[j] < data[j-1] {
				v1 := data[j]
				v2 := data[j-1]

				data[j] = v2
				data[j-1] = v1
			} else {
				continue
			}
		}
	}
	return data
}
