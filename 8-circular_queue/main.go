package main

import (
	"errors"
	"fmt"
	"os"
)

// Queue 管理環形佇列的結構
type Queue struct {
	maxQueueSize int
	creatQ       [5]int
	front        int
	rear         int
}

// Push 入佇列
func (q *Queue) Push(item int) (err error) {
	if q.IsFull() {
		return errors.New("佇列已滿！")
	}
	q.creatQ[q.rear] = item
	q.rear = (q.rear + 1) % q.maxQueueSize
	return
}

// Pop 出佇列
func (q *Queue) Pop() (item int, err error) {
	if q.IsEmpty() {
		return 0, errors.New("佇列為空！")
	}
	item = q.creatQ[q.front]
	q.front = (q.front + 1) % q.maxQueueSize
	return
}

// Show 顯示佇列
func (q *Queue) Show() {
	if q.IsEmpty() {
		fmt.Println("佇列為空！")
	}
	temp := q.front
	for i := 0; i < q.Size(); i++ {
		fmt.Printf("array[%d]:%d\t", temp, q.creatQ[temp])
		temp = (temp + 1) % q.maxQueueSize
	}
}

// IsFull 判斷佇列是否已滿
func (q *Queue) IsFull() bool {
	return (q.front+1)%q.maxQueueSize == q.front
}

// IsEmpty 判斷佇列是否為空
func (q *Queue) IsEmpty() bool {
	return q.front == q.rear
}

// Size 查詢有多少個佇列
func (q *Queue) Size() int {
	return (q.rear + q.maxQueueSize - q.front) % q.maxQueueSize
}

func main() {
	q := &Queue{
		maxQueueSize: 5,
		front:        0,
		rear:         0,
	}

	var xz string
	var number int
	for {
		fmt.Println()
		fmt.Println("1.新增佇列請輸入add")
		fmt.Println("2.獲取佇列請輸入get")
		fmt.Println("3.顯示佇列請輸入show")
		fmt.Println("4.輸入exit退出")
		fmt.Scanln(&xz)

		switch xz {
		case "add":
			fmt.Println("輸入你要入列的數：")
			fmt.Scanln(&number)
			err := q.Push(number)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Printf("加入佇列成功!\n")
			}
		case "get":
			val, err := q.Pop()
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Printf("佇列已取出：%d", val)
			}

		case "show":
			q.Show()
			fmt.Println()
		case "exit":
			os.Exit(0)
		}
	}
}
